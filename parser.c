/* parser.c by Erin Wilson, Marielle Foster, and Adam Canady
   CS 251 - Dave Musicant - Winter 2014

   This program takes input from stdin and outputs a parsed version of that file
   after building a parse tree.*/

   // problems: (FIXED:!!!! too many close parens causes segfault)
   // () and '()

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "tokenizer.h"
#include "parser.h"

ConsCell* getTokens(){
    char *current_line = malloc(sizeof(char)*256);
    ConsCell *current_head = NULL;
    // loop through stdin line by line, assigning line to current_line
    while( fgets(current_line, 256 , stdin) ) 
    {
        // make sure line doesn't have a newline in it
        int length = strlen(current_line);
        if (current_line[length-1] == '\n') current_line[length-1] = '\0';

        // assign current_head to point to the most recently tokenized node in our linked_list
        current_head = tokenize(current_line, current_head); 
    }

    current_head = reverse_list(current_head);
    free(current_line);
    return current_head;
}

void popFromFront(ConsCell **listOfCells, ConsCell **front){
    // assert(*listOfCells != NULL);
    // printf("X\n");
    *front = *listOfCells;
    // printf("Y\n");
    if (//listOfCells != NULL 
        (*listOfCells)->cdr != NULL 
        && (*listOfCells)->cdr->cons != NULL){
      // printf("nonnull cell\n");
        *listOfCells = (*listOfCells)->cdr->cons;
    } else { *listOfCells = NULL;}

    // printf("Z\n");
}

void pushToStack(ConsCell **token, ConsCell **tree){
    (*token)->cdr->cons = *tree; // check this later
    *tree = *token; // check this later
}

int distanceToOpen(ConsCell **tree){
    int i = 0;
    ConsCell *item = *tree;

    while(item != NULL && item->car->type != openType){
        item = item->cdr->cons;
        i++;
    }

    return i;
}

// bundle should create a cons cells with Car's type as a consType
ConsCell* bundle(ConsCell **tree){
    ConsCell *tempList = NULL;
    
    // If we're at a ()
    if(distanceToOpen(tree) == 0){

    }

    while (*tree){
        // puts poppedCell at beginning of tempList
        ConsCell *poppedCell;
        popFromFront(tree, &poppedCell);
        pushToStack(&poppedCell, &tempList);

        if (tempList->car->type == openType){
            // Make new value struct
            Value *juncValue = malloc(sizeof(Value));
            juncValue->type = consType;
            if(tempList != NULL){
                juncValue->cons = tempList->cdr->cons;
            } else {
                juncValue->cons = NULL;
            }
            free(tempList->car);
            free(tempList->cdr);
            free(tempList);
            // Make a null cons pointer
            Value *juncCdrValue = malloc(sizeof(Value));
            juncCdrValue->type = consType;
            juncCdrValue->cons = NULL;

            //Make new consCell
            ConsCell *junctionCell = malloc(sizeof(ConsCell));
            junctionCell->car = juncValue;
            junctionCell->cdr = juncCdrValue;

            return junctionCell;
        }
    }
    // Got to the beginning of the tree without finding a matching open paren... sad
    if(tempList != NULL){
        freeTree(tempList);
    }
    syntaxError("Too Many Close Parens."); // Too many close parens
    return NULL;
}

void addToParseTree(ConsCell **tree, ConsCell **token, int *depth){
    if ((*token)->car->type == closeType){
        free((*token)->car);
        free((*token)->cdr);
        free((*token));
        ConsCell *newBundle;
        newBundle = bundle(tree);
        pushToStack(&newBundle, tree);
        (*depth)--;
    } else {
        if ((*token)->car->type == openType) (*depth)++;
        pushToStack(token, tree);
    }
}
void printValue(Value *val){
  if(val == NULL) return;
  switch(val->type){
    case integerType:
      printf("%i", val->integerValue);
      break;
    case floatType:
      printf("%f", val->floatValue);
      break;
    case symbolType:
      printf("%s", val->symbolValue); // valgrind complains here
      break;
    case stringType:
      printf("%s", val->stringValue);
      break;
    case booleanType:
      printf("%s", val->booleanValue?"#t":"#f");
      break;
    case consType:
        // printf("8.2----\n");
      printTree(val->cons);
      // printf("8.3----\n");
      break;
      // printf("\n");
    case closureType:
      printf("#<procedure>");
      break;
    case bindingType:
      printf("Binding: %s ", val->binding->varName);
      printValue(val->binding->value);
      break;

  }
  return;
}

void printTree(ConsCell *tree){
    if(tree == NULL) return;
    // printf("A\n");
    ConsCell *curCell = tree;
    // printf("B\n");
    // printf("C2\n");
    
    while (curCell != NULL) { // while current bundle has not ended...
        // printf("C4\n");

        // Print out the first part if it exists
        if(curCell->car != NULL){
            switch(curCell->car->type){
                case integerType:
                case floatType:
                case symbolType:
                case stringType:
                case booleanType:
                case bindingType:
                case closureType:
                    printValue(curCell->car);
                    break;
                // if you've reached another bundle, recursively print the bundle!
                case consType:
                    printf("(");
                    if(curCell->car->cons != NULL){
                        printTree(curCell->car->cons);
                    }
                    printf(")");
                    break;
            }
        }

        // Print out the second part if it exists
        if(curCell->cdr != NULL){
            switch(curCell->cdr->type){
                case consType:
                  // printTree(curCell->cdr->cons);
                  // go to the next cell on this level if there is one, otherwise get out of the loop
                  if(curCell->cdr != NULL 
                     && curCell->cdr->cons != NULL){ 
                      curCell = curCell->cdr->cons; 
                      printf(" ");
                  } else { curCell = NULL; }
                  continue; // wrap around the loop again

                default:
                  printf(" . ");
                  printValue(curCell->cdr);
                  break;
            }
        }
      break; // leave the loop
    }
    // printf("D\n");
}

// rethink this
void freeTree(ConsCell *tree){
    ConsCell *tempTree = tree; 
    ConsCell *curCell;
    // printf("tree: %p curcell: %p\n", &tree, &curCell);
    if(tree != NULL){
      popFromFront(&tree, &curCell);
    } else { return; }
    
    if (curCell != NULL) { // while current bundle has not ended...
        if(curCell->car != NULL){
          switch(curCell->car->type){
              case symbolType:
                  free(curCell->car->symbolValue);
                  break;
              case stringType:
                  free(curCell->car->stringValue);
                  break;
              
              // This case probably doesn't work. // it may be fixed now...
              case bindingType:
                  freeBindings(curCell);
                  // if(curCell->car->binding != NULL){
                  //     free(curCell->car->binding);
                  //     free(curCell->car);

                  //     if (curCell->cdr != NULL && curCell->cdr->cons != NULL){
                  //         freeTree(curCell->cdr->cons);
                  //     }
                  //     free(curCell->cdr);
                  //     break;
                  // }                
                  break;
              case consType:
                  if(curCell->car->cons != NULL){
                      freeTree(curCell->car->cons);
                  }
                  break;
              case closureType:
                  free(curCell->car->closure->args);
                  curCell->car->closure->args = NULL;

                  free(curCell->car->closure->funcBody);
                  curCell->car->closure->funcBody = NULL;

                  freeTree((ConsCell*) curCell->car->closure->frame); //FREE THE ENVIRONMENT
                  curCell->car->closure->frame = NULL;

                  break;
          }
        }
        
        // go to the next cell on this level if there is one, otherwise get out of the loop
        if(curCell->cdr != NULL && curCell->cdr->cons != NULL){ 
            freeTree(curCell->cdr->cons);
            curCell->cdr->cons = NULL;
            // printf(" ");
        // } else { 
        //     curCell = NULL;
        //     printf("freed on level\n"); 
        }
        if(tempTree != NULL && tempTree->car != NULL) { free(tempTree->car); tempTree->car = NULL; }
        if(tempTree != NULL && tempTree->cdr != NULL) { free(tempTree->cdr); tempTree->cdr = NULL; }
        if(tempTree != NULL) { free(tempTree); tempTree = NULL;}
    }
    
    //curCell == NULL
}

// makes sure our bindings are taken care of when we want to get rid of a stack frame
void freeBindings(ConsCell *varChain){
  if(varChain != NULL
     && varChain->car != NULL
     && varChain->car->binding != NULL){

    // get rid of this binding's varName and value
    free(varChain->car->binding->varName);

    switch(varChain->car->binding->value->type){
        case symbolType:
            free(varChain->car->binding->value->symbolValue);
            break;
        case stringType:
            free(varChain->car->binding->value->stringValue);
            break;
        case consType:
            if(varChain->car->binding->value->cons != NULL){
                freeTree(varChain->car->binding->value->cons);
            }
            break;


        // case closureType:
        //   if (varChain->car->binding->value->closure == NULL) return;

        //   if (varChain->car->binding->value->closure->args != NULL)
        //     freeTree(varChain->car->binding->value->closure->args);
        //   if (varChain->car->binding->value->closure->funcBody != NULL)
        //     freeTree(varChain->car->binding->value->closure->funcBody);
        //   if (varChain->car->binding->value->closure->frame != NULL)
        //     freeTree((ConsCell*) varChain->car->binding->value->closure->frame); //FREE THE ENVIRONMENT

        //   if(varChain->car->binding->value->closure != NULL)
        //     free(varChain->car->binding->value->closure);

        //   break;
        case closureType:
            if (varChain->car->binding->value->closure->args != NULL)
              freeTree(varChain->car->binding->value->closure->args);
            varChain->car->binding->value->closure->args = NULL;

            if (varChain->car->binding->value->closure->funcBody != NULL)
              freeTree(varChain->car->binding->value->closure->funcBody);
            varChain->car->binding->value->closure->funcBody = NULL;

            if (varChain->car->binding->value->closure->frame != NULL)
              freeTree(varChain->car->binding->value->closure->frame); //FREE THE ENVIRONMENT
            varChain->car->binding->value->closure->frame = NULL;

            if(varChain->car->binding->value->closure != NULL)
              free(varChain->car->binding->value->closure);
            varChain->car->binding->value->closure = NULL;

            break;
    }

    if(varChain->car->binding->value != NULL)
      free(varChain->car->binding->value);
    varChain->car->binding->value = NULL;

    free(varChain->car->binding);
    varChain->car->binding = NULL;

    free(varChain->car);
    varChain->car = NULL;
    
    if(varChain->cdr != NULL
       && varChain->cdr->cons != NULL){
      freeBindings(varChain->cdr->cons);
    }
    
    if(varChain != NULL 
       && varChain->cdr != NULL 
       && varChain->cdr->cons != NULL){
      free(varChain->cdr->cons);
      varChain->cdr->cons = NULL;
    } else if (varChain != NULL
               && varChain->cdr != NULL){
      free(varChain->cdr);
      varChain->cdr = NULL;

    }

    if(varChain != NULL) free(varChain);
    varChain = NULL;
    
  }
}


void syntaxError(char* message){
    printf("Syntax Error: %s\n", message);
    exit(0);
}