#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "tokenizer.h"

int main(int argc, char *argv[])
{
    char *current_line = malloc(sizeof(char)*256);
    //int line_num = 0;
    ConsCell *current_head = NULL;
    // loop through stdin line by line, assigning line to current_line
    while( fgets(current_line, 256 , stdin) ) 
    {
        // make sure line doesn't have a newline in it
        int length = strlen(current_line);
        if (current_line[length-1] == '\n') current_line[length-1] = '\0';

        // assign current_head to point to the most recently tokenized node in our linked_list
        current_head = tokenize(current_line, current_head); 

        // printf("Line: %s\n", current_line);
        //line_num++;
    }

    current_head = reverse_list(current_head);
    print_list(current_head);

    // cleanup stuff here
    free(current_line);
    cleanup_list(current_head);
}

