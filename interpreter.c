// #adamcanady #erinwilson #mariellefoster
// #trendingOnTwitter


#include "tokenizer.h" // to get structs
#include "parser.h"
#include "interpreter.h" // to get function declarations
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

// TODO: Fix memory leak on define (probably something with the cons cell that is called in freeBindings in the interpreter_main file)
// and do lambda stuff

// free closures

char* lolDaveTroll = "<3";

// we expect to give this the first item after an open paren,
// for example (+ a b), the first item would be +
// therefore this first item is always a function or a special form
// (let, if, etc.)
Value* evalExpr(ConsCell *expr, Environment *env, Environment **globalEnv) {
  if(expr == NULL){ evaluationError(0); }
  assert(expr->car != NULL);

  // expect first item to be a function (symbolType or consType)!
  if (expr->car->type != symbolType
      && expr->car->type != consType) { evaluationError(1); }

  // ****************************************************************************  
  // If you've hit a symbolType after open paren, figure out what it means!
  if(expr->car->type == symbolType) {
    char* symbol = expr->car->symbolValue;

    // check special forms
    // --------------------------------------------------------------------------
    // let
    //potentially restructure into another function
    if (!strcmp(symbol, "let")) {
      // create environment
      assert(expr->cdr->type == consType);
      env = createEnvironment(expr->cdr->cons->car->cons, env, globalEnv);

      assert(expr->cdr->cons->cdr->cons != NULL); // make sure we have a body
      Value *body = expr->cdr->cons->cdr->cons->car; // this is probably wrong. Should be a value?

      Value *tempEvaluatedValue;
      // evaluate body
        // check if body is a symbol or a cons, and react appropriately
      switch (body->type){
        case consType:
          // Value *tempValue = eval(body, env)
          // env = cleanupEnv(env, oldEnv);
          // return tempValue;
          tempEvaluatedValue = evalValue(body, env, globalEnv);
          break;
        case symbolType:
          tempEvaluatedValue = resolveVariable(body, env, *globalEnv);
          break;
        case booleanType:
        case stringType:
        case integerType:
        case floatType:
          tempEvaluatedValue = body;
          break;
      }

      // cleanup bindings
      assert(env != NULL);
      assert(env->car != NULL);
      assert(env->car->cons != NULL);

      // freeBindings(env->car->cons);
      free(env->car->cons);
      free(env->car);
      if(env->cdr != NULL) free(env->cdr);
      free(env);

      return tempEvaluatedValue;

    }

    // --------------------------------------------------------------------------
    // let*
    if (!strcmp(symbol, "let*")) {
      // create environment
      assert(expr->cdr->type == consType);
      env = createLetStarEnvironment(expr->cdr->cons->car->cons, env, globalEnv);

      assert(expr->cdr->cons->cdr->cons != NULL); // make sure we have a body
      ConsCell *body = expr->cdr->cons->cdr->cons;

      Value *tempEvaluatedValue;
      // evaluate body
        // check if body is a symbol or a cons, and react appropriately
      switch (body->car->type){
        case consType:
          // Value *tempValue = eval(body, env)
          // env = cleanupEnv(env, oldEnv);
          // return tempValue;
          tempEvaluatedValue = evalExpr(body, env, globalEnv);
          break;
        case symbolType:
          tempEvaluatedValue = resolveVariable(body->car, env, *globalEnv);
          break;
        case booleanType:
        case stringType:
        case integerType:
        case floatType:
          tempEvaluatedValue = body->car;
          break;
      }
      // cleanup bindings
      assert(env != NULL);
      assert(env->car != NULL);
      assert(env->car->cons != NULL);

      // freeBindings(env->car->cons);
      free(env->car->cons);
      free(env->car);
      if(env->cdr != NULL) free(env->cdr);
      free(env);

      return tempEvaluatedValue;
    }

    // --------------------------------------------------------------------------
    // letrec
    if (!strcmp(symbol, "letrec")) {
      // create environment
      assert(expr->cdr->type == consType);
      env = createLetRecEnvironment(expr->cdr->cons->car->cons, env, globalEnv);

      assert(expr->cdr->cons->cdr->cons != NULL); // make sure we have a body
      ConsCell *body = expr->cdr->cons->cdr->cons;

      Value *tempEvaluatedValue;
      // evaluate body
        // check if body is a symbol or a cons, and react appropriately
      switch (body->car->type){
        case consType:
          // Value *tempValue = eval(body, env)
          // env = cleanupEnv(env, oldEnv);
          // return tempValue;
          tempEvaluatedValue = evalExpr(body, env, globalEnv);
          break;
        case symbolType:
          tempEvaluatedValue = resolveVariable(body->car, env, *globalEnv);
          break;
        case booleanType:
        case stringType:
        case integerType:
        case floatType:
          tempEvaluatedValue = body->car;
          break;
      }
      // cleanup bindings
      assert(env != NULL);
      assert(env->car != NULL);
      assert(env->car->cons != NULL);

      // freeBindings(env->car->cons);
      free(env->car->cons);
      free(env->car);
      if(env->cdr != NULL) free(env->cdr);
      free(env);

      return tempEvaluatedValue;

    }

    // --------------------------------------------------------------------------
    // cond
    if (!strcmp(symbol, "cond")) {
      return evalCond(expr->cdr, env, globalEnv);


    }

    // --------------------------------------------------------------------------
    // and
    if (!strcmp(symbol, "and")) {
      return evalAnd(expr->cdr, env, globalEnv);

    }

    // --------------------------------------------------------------------------
    // or
    if (!strcmp(symbol, "or")) {
      return evalOr(expr->cdr, env, globalEnv);

    }

    // --------------------------------------------------------------------------
    // begin
    if (!strcmp(symbol, "begin")) {
      // if given no args, just return null
      if(expr->cdr == NULL){
        return (Value*) NULL;
      }

      // otherwise, iterate through args until get to end of begin stmt, return result of final expr
      while(expr->cdr->cons != NULL){
        evalValue(expr->cdr->cons->car, env, globalEnv); // what do you do with the return value???
        expr = expr->cdr->cons; 
      }

      return evalValue(expr->car, env, globalEnv);


      //return (Value*) NULL;

    }

    // --------------------------------------------------------------------------
    // if
    if (!strcmp(symbol, "if")) {
      assert(expr->cdr != NULL);
      return evalIf(expr->cdr->cons, env, globalEnv);
    }

    // --------------------------------------------------------------------------
    // quote
    if (!strcmp(symbol, "quote")) {
      assert(expr->cdr != NULL);
      if (lengthOfList(expr->cdr->cons) != 1){
        evaluationError(2);
      }
      return evalQuote(expr->cdr);
    }
    
    // --------------------------------------------------------------------------
    // define
    if(!strcmp(symbol, "define")){
      assert(expr->cdr != NULL);
      if (lengthOfList(expr->cdr->cons) != 2){
        evaluationError(3);
      }
      // assert all these exist
      // double check later
      assert(expr->cdr->cons != NULL);
      assert(expr->cdr->cons->car != NULL);
      assert(expr->cdr->cons->cdr != NULL);
      assert(expr->cdr->cons->cdr->cons != NULL);
      assert(expr->cdr->cons->cdr->cons->car != NULL);


      // Get varName
      Value *var = expr->cdr->cons->car;
      int varLength = strlen(var->symbolValue);
      char* varName = malloc(sizeof(char)*varLength+1); // length and null terminator
      strcpy(varName, var->symbolValue);
      // Get expression
      Value *expressionEval = evalValue(expr->cdr->cons->cdr->cons->car, env, globalEnv);
      Value *expression = copyValue(expressionEval);

      if (expressionEval->copied == 1) freeReturnValue(expressionEval);
      
      Binding *currentBinding = malloc(sizeof(Binding));
      currentBinding->varName = varName;
      currentBinding->value = expression;
      ConsCell *previousBindingCons = (*globalEnv)->car->cons;
      

      // Put new binding at the head of the linked list
      ConsCell *currentBindingCons = malloc(sizeof(ConsCell));
      currentBindingCons->car = malloc(sizeof(Value));
      currentBindingCons->car->type = bindingType;
      currentBindingCons->cdr = malloc(sizeof(Value));

      currentBindingCons->car->binding = currentBinding;
      currentBindingCons->cdr->cons = previousBindingCons;

      (*globalEnv)->car->cons = currentBindingCons;
      return (Value *) NULL; // get out of the funct`ion NAAWWWWW!!
    }

    // --------------------------------------------------------------------------
    // lambda
    if(!strcmp(symbol, "lambda")){
      assert(expr->cdr != NULL); // Make sure it's not just (lambda)
      assert(expr->cdr->cons != NULL);
      assert(expr->cdr->cons->car != NULL);
      // assert(expr->cdr->cons->car->cons != NULL); // make sure it has args // not necessary
      assert(expr->cdr->cons->cdr != NULL);
      assert(expr->cdr->cons->cdr->cons != NULL);

      Closure *newClosure = malloc(sizeof(Closure));

      if(expr->cdr->cons->car->cons != NULL){
        newClosure->args = copyCons(expr->cdr->cons->car->cons);
      } else {
        newClosure->args = NULL;
      }

      // funcbody could just be a single variable
      if(expr->cdr->cons->cdr->cons->car->type == consType)
      newClosure->funcBody = copyCons(expr->cdr->cons->cdr->cons->car->cons);
      else {
        newClosure->funcBody = malloc(sizeof(ConsCell));
        newClosure->funcBody->car = copyValue(expr->cdr->cons->cdr->cons->car);
        newClosure->funcBody->cdr = NULL;
      }
      newClosure->frame = copyEnvironment(env);

      Value *closureValue = malloc(sizeof(Value));
      closureValue->copied = 1;
      closureValue->type = closureType;
      closureValue->closure = newClosure;

      return closureValue;
    }

    // --------------------------------------------------------------------------
    // plus
    if(!strcmp(symbol, "+")){
      return evalPlus(expr->cdr, 0, integerType, env, globalEnv);
    }

    // --------------------------------------------------------------------------
    // *
    if (!strcmp(symbol, "*")) {

      return evalMult(expr->cdr, 1, integerType, env, globalEnv);

    }

    // --------------------------------------------------------------------------
    // -
    if (!strcmp(symbol, "-")) {

      assert(expr->cdr != NULL);
      if (lengthOfList(expr->cdr->cons) != 2){
        evaluationError(4);
      }
      float difference;
      VALUE_TYPE type = integerType;
      assert(expr->cdr->cons != NULL);
      assert(expr->cdr->cons->car != NULL);
      Value *minuend = evalValue(expr->cdr->cons->car, env, globalEnv);

      if(minuend->type != floatType && minuend->type != integerType){ evaluationError(4); }  
      if(minuend->type == floatType){ 
        type = floatType; 
        difference = minuend->floatValue;
      }
      else{
        difference = minuend->integerValue;
      }
      assert(expr->cdr->cons->cdr != NULL);
      assert(expr->cdr->cons->cdr->cons != NULL);
      assert(expr->cdr->cons->cdr->cons->car != NULL);
      Value *subtrahend = evalValue(expr->cdr->cons->cdr->cons->car, env, globalEnv);

      if(subtrahend->type != floatType && subtrahend->type != integerType){ evaluationError(4); }
      if(subtrahend->type == floatType){ 
        type = floatType; 
        difference = difference - subtrahend->floatValue;
      }
      else{
        difference = difference - subtrahend->integerValue;
      }
      Value *returnDiff = malloc(sizeof(Value));
      returnDiff->copied = 1;
      if(type == floatType){
        returnDiff->type = floatType;
        returnDiff->floatValue = difference;
      }
      else{
        returnDiff->type = integerType;
        returnDiff->integerValue = (int)difference;
      }


      return returnDiff;
    }

    // --------------------------------------------------------------------------
    // /
    if (!strcmp(symbol, "/")) {

      assert(expr->cdr != NULL);
      if (lengthOfList(expr->cdr->cons) != 2){
        evaluationError(4);
      }
      float quotient;
      VALUE_TYPE type = integerType;
      assert(expr->cdr->cons != NULL);
      assert(expr->cdr->cons->car != NULL);
      Value *dividend = evalValue(expr->cdr->cons->car, env, globalEnv);

      if(dividend->type != floatType && dividend->type != integerType){ evaluationError(4); }  
      if(dividend->type == floatType){ 
        type = floatType; 
        quotient = dividend->floatValue;
      }
      else{ quotient = dividend->integerValue; }
      assert(expr->cdr->cons->cdr != NULL);
      assert(expr->cdr->cons->cdr->cons != NULL);
      assert(expr->cdr->cons->cdr->cons->car != NULL);
      Value *divisor = evalValue(expr->cdr->cons->cdr->cons->car, env, globalEnv);

      if(divisor->type != floatType && divisor->type != integerType){ evaluationError(4); }

      if(divisor->type == floatType){ 
        type = floatType; 
        quotient = quotient / (divisor->floatValue);
      }
      else{
        if(type == integerType && (int)quotient%(divisor->integerValue) != 0){
          quotient = quotient / (float)(divisor->integerValue);
          type = floatType;
        }
        else{ quotient = quotient / (float)(divisor->integerValue); }
      }
      Value *returnQuo = malloc(sizeof(Value));
      returnQuo->copied = 1;
      if(type == floatType){
        returnQuo->type = floatType;
        returnQuo->floatValue = quotient;
      }
      else{
        returnQuo->type = integerType;
        returnQuo->integerValue = (int)quotient;
      }
      return returnQuo;
    }

    // --------------------------------------------------------------------------
    // modulo
    if (!strcmp(symbol, "modulo")) {
      // Make sure the function received exactly 2 arguments
      if(lengthOfList(expr) != 3){ evaluationError(4); }
      
      // Get the first and second args after "modulo"
      Value *arg1 = evalValue(expr->cdr->cons->car, env, globalEnv);
      Value *arg2 = evalValue(expr->cdr->cons->cdr->cons->car, env, globalEnv);

      // Check to make sure both args are ints
      if (arg1->type != integerType || arg2->type != integerType) { evaluationError(4); }

      // While arg2 is bigger than arg 1, subtract arg2 from arg1
      while (arg1->integerValue >= arg2->integerValue){
        arg1->integerValue -= arg2->integerValue;
      }

      return arg1;

    }

    // --------------------------------------------------------------------------
    // <=
    if (!strcmp(symbol, "<=")) {
      return evalBoolStmt(expr, leq, env, globalEnv);
    }

    // --------------------------------------------------------------------------
    // >=
    if (!strcmp(symbol, ">=")) {
      return evalBoolStmt(expr, geq, env, globalEnv);
    }

    // --------------------------------------------------------------------------
    // =
    if (!strcmp(symbol, "=")) {
      return evalBoolStmt(expr, eq, env, globalEnv);
    }

    
    // --------------------------------------------------------------------------
    // null?
    if(!strcmp(symbol, "null?")){


      if(lengthOfList(expr) != 2){ evaluationError(4); }
      Value *newBool = malloc(sizeof(Value));
      newBool->type = booleanType;
      Value *newVal = evalValue(expr->cdr->cons->car, env, globalEnv);
      if(newVal->cons->car->cons == NULL){
        newBool->booleanValue = true;
        return newBool; 
      } else {
        
        newBool->booleanValue = false;
        return newBool; 
      }
    }

    // --------------------------------------------------------------------------
    // car
    if(!strcmp(symbol, "car")){
      // If car is given no arguments, raise error
      if(lengthOfList(expr) < 2){ evaluationError(5); }
      
      // newVal is the item in the cell following "car"
      Value *newVal = evalExpr(expr->cdr->cons, env, globalEnv);

      return newVal->cons->car->cons->car;
    }

    // --------------------------------------------------------------------------
    // cdr
    if(!strcmp(symbol, "cdr")){
      if(lengthOfList(expr) < 2){ evaluationError(6); }

      // newVal is the item in the cell following "cdr", ex. (cdr (cons 1 2)), newVal would contain
      // a value that points to the cons cell (1 2)
      Value *newVal = evalExpr(expr->cdr->cons, env, globalEnv);

      Value *cdrList = newVal->cons->car->cons->cdr;
      if(cdrList->type == consType){
        Value *returnWrap = malloc(sizeof(Value));   // HOW DO YOU FREE THESE??
        returnWrap->copied = 0;
        Value *valueWrap = malloc(sizeof(Value));
        ConsCell *consWrap = malloc(sizeof(ConsCell)); // this used to be Value, but I changed it. (AC)

        // point to cdr with a Value struct
        valueWrap->type = consType;
        valueWrap->cons = cdrList->cons;

        // put value struct in ConsCell
        consWrap->car = valueWrap;
        consWrap->cdr = (Value*) NULL;

        // point to ConsCell with a Value to return (must return a Value)
        returnWrap->type = consType;
        returnWrap->cons = consWrap;

        return returnWrap;

      } else {
        return cdrList;
      }
    }   

    // --------------------------------------------------------------------------
    // cons
    if(!strcmp(symbol, "cons")){
      if(lengthOfList(expr) != 3){ evaluationError(61); } // need 2 things (car, cdr) and the symbol cons

      Value *car = evalValue(expr->cdr->cons->car, env, globalEnv);
      Value *cdr = evalValue(expr->cdr->cons->cdr->cons->car, env, globalEnv);

      ConsCell *newCons = malloc(sizeof(ConsCell));
      newCons->car = car;
      newCons->cdr = cdr;

      Value *consWrapper = malloc(sizeof(Value));
      consWrapper->type = consType;
      consWrapper->cons = newCons;

      ConsCell *returnCons = malloc(sizeof(ConsCell));
      returnCons->car = consWrapper;
      returnCons->cdr = (Value*) NULL;

      Value *returnVal = malloc(sizeof(Value));
      returnVal->copied = 0;
      returnVal->type = consType;
      returnVal->cons = returnCons;

      return returnVal;
    }

    // Extra optional functions: -----------------------------------------------
    // >
    if (!strcmp(symbol, ">")) {
      return evalBoolStmt(expr, gt, env, globalEnv);
    }

    // --------------------------------------------------------------------------
    // <
    if (!strcmp(symbol, "<")) {
      return evalBoolStmt(expr, lt, env, globalEnv);
    }

    // --------------------------------------------------------------------------
    // length
    if (!strcmp(symbol, "length")) {
      // Make sure it was given args
      if(lengthOfList(expr) < 2) {evaluationError(20);}

      // Make sure you were actually looking at a list
      if(expr->cdr->cons->car->type != consType || 
         expr->cdr->cons->car->cons->car->type != symbolType ||
         strcmp(expr->cdr->cons->car->cons->car->symbolValue, "quote")){
          evaluationError(21);
         }

      // If quote wasn't given any args
      if(expr->cdr->cons->car->cons->cdr->cons == NULL){evaluationError(22);}

      // WOOO, formatted properly!
      int len = lengthOfList(expr->cdr->cons->car->cons->cdr->cons->car->cons);
      Value *returnVal = malloc(sizeof(Value));
      returnVal->type = integerType;
      returnVal->integerValue = len;

      return returnVal;
    }


    /* If we've reached this point we've determined this symbol is not one of our 
    built in functions and it's not a special form, so if we're going to run it, 
    it must be a closure that is stored in a variable somewhere. 

    Now we check through all our bindings/environments to find it */

    Value* resolvedSymbol = resolveVariable(expr->car, env, *globalEnv);

    // if it's a lambda, deal with it
    if(resolvedSymbol->type == closureType){
      return evaluateLambda(resolvedSymbol, expr, env, globalEnv);
    }

    return evalValue(expr->car, env, globalEnv);

    // return (Value *) NULL;
    // otherwise, get the arguments and put everything together
  }

  // ****************************************************************************  
  // If you've hit a consType after an open paren, just evaluate it!
  if (expr->car->type == consType){
    Value* returnedVal = evalExpr(expr->car->cons, env, globalEnv);
    if(returnedVal->type == closureType){
      return copyValue(evaluateLambda(returnedVal, expr, env, globalEnv));
    } else { return returnedVal; }
  }

  // ****************************************************************************  
  // If the first item after an open paren was not a symbolType or consType... something weird happened
  else { printf("UH OHHHHH"); }
  return (Value *) NULL;
}

Value *evaluateLambda(Value *closureVal, ConsCell *expr, Environment *env, Environment **globalEnv){
  assert(closureVal != NULL);
  assert(closureVal->type == closureType);

  Value *lambda = closureVal;
  ConsCell *formalParameters = lambda->closure->args;
  ConsCell *funcBody = lambda->closure->funcBody;
  ConsCell *localEnvironment = lambda->closure->frame;

  int expectedArguments = lengthOfList(formalParameters);
  int numActualParameters;
  if(expr->cdr == NULL || expr->cdr->type != consType) { numActualParameters = 0; }
  else { numActualParameters = lengthOfList(expr->cdr->cons); }

  //change if currying is a thing
  if(expectedArguments != numActualParameters){
    evaluationError(75); 
  }

  ConsCell *actualParameters = NULL;
  if(numActualParameters > 0){
    actualParameters = expr->cdr->cons;
  }

  // probably create some kind of envrionment by correlating each argument with each variable (symbol?)    
  // attach it to the current environment
  env = createLambdaEnv(numActualParameters, formalParameters, actualParameters, localEnvironment, env, globalEnv);

  // then evalExpr on the body (copy the value, then free the bindings that were temporarily created) and return it
  // not sure if this will work for embedded functions. we may have to rethink our freeing strategy
  Value *lambdaValue = evalExpr(funcBody, env, globalEnv);

  // printEnvironment(env);

  // if(numActualParameters > 0){
  //   Environment *parameterEnvironment = env;
  //   parameterEnvironment->cdr = NULL;
  //   // freeTree(parameterEnvironment);
  //   free(parameterEnvironment);
  // }

  return lambdaValue;
}

/* This function will do one of two things:
 *   1. If a function takes parameters, it'll do the following
 *      - create a frame that contains the actual parameters attached to the formal parameters
 *      - attach that frame before the localEnvironment of the function
 *      - attach that frame to the env that the function will be run in.
 *   2. If a function doesn't take parameters
 *      - attach the localEnvironment to the front of the env and return that frame
 *
 */
Environment *createLambdaEnv(int numActualParameters, 
                             ConsCell* funcArgs, 
                             ConsCell* actualParameters, 
                             Environment* localEnvironment, 
                             Environment* env, 
                             Environment **globalEnv){

  if(numActualParameters > 0){
    // attach funcArgs to actualParameters
    ConsCell *parameters = NULL;
    while (funcArgs != NULL && actualParameters != NULL){
      // create binding
      Binding *thisBinding = malloc(sizeof(Binding));
      thisBinding->varName = funcArgs->car->symbolValue; 
      thisBinding->value = evalValue(actualParameters->car, env, globalEnv);

      // create encompassing Value
      Value *nextParameterValue = malloc(sizeof(Value));
      nextParameterValue->type = bindingType;  //--Would this need to come before the previous block?
      nextParameterValue->binding = thisBinding;

      Value *previousParameterValue = malloc(sizeof(Value));
      previousParameterValue->type = consType;
      previousParameterValue->cons = parameters;

      // attach them together
      ConsCell *nextParameterCons = malloc(sizeof(ConsCell));
      nextParameterCons->car = nextParameterValue;
      nextParameterCons->cdr = previousParameterValue;

      parameters = nextParameterCons; // value or cons?

      if (funcArgs->cdr != NULL){
        funcArgs = funcArgs->cdr->cons;  
      } else {
        funcArgs = NULL;
      }
      if (actualParameters->cdr != NULL){
        actualParameters = actualParameters->cdr->cons;  
      } else {
        actualParameters = NULL;
      }
    }

    // wrap parameters in a Value
    Value *parameterEnvironmentValue = malloc(sizeof(Value));
    parameterEnvironmentValue->type = consType;
    parameterEnvironmentValue->cons = parameters;

    // wrap localEnvironment in a Value
    Value *localEnvironmentValue = malloc(sizeof(Value));
    localEnvironmentValue->type = consType;
    localEnvironmentValue->cons = (ConsCell*) localEnvironment;

    // create new parameterEnvironment to put before localEnvironment
    Environment *parameterEnvironment = malloc(sizeof(Environment));
    parameterEnvironment->car = parameterEnvironmentValue;
    parameterEnvironment->cdr = localEnvironmentValue;

    if (env != NULL){
      Environment *temp = parameterEnvironment;
      while(temp->cdr != NULL){
        if(temp->cdr->type == consType 
           && temp->cdr->cons != NULL 
           && temp->cdr->cons != temp){ temp = temp->cdr->cons; }
        else { break; }
      }

      temp->cdr = malloc(sizeof(Value));
      temp->cdr->type = consType;
      temp->cdr->cons = env;
    }

    return parameterEnvironment;
  } else {
    if (env != NULL){
      Environment *temp = localEnvironment;
      while(temp != NULL 
            && temp->cdr != NULL){
        temp = temp->cdr->cons;
      }

      temp->cdr = malloc(sizeof(Value));
      temp->cdr->type = consType;
      temp->cdr->cons = env;
    }  
    return localEnvironment;
  }
}

Environment *cleanOneFrame(Environment *env){
  assert(env->car != NULL);
  assert(env->car->type == consType);
  assert(env->car->cons != NULL);

  Environment *environmentToClean = env->car->cons;
  Environment *envToReturn;
  if(env->cdr != NULL && env->cdr->type == consType){
    envToReturn = (Environment*) env->cdr->cons;
  } else { envToReturn = (Environment*) NULL; }

  freeTree(env->car->cons);
  free(env->car);
  free(env);

  return envToReturn;
}

Value *evalCond(Value *args, Environment *env, Environment **globalEnv){
  
  if(args->cons == NULL){
    return (Value *) NULL;
  }
  assert(args->cons->car != NULL);
  assert(args->cons->cdr != NULL);

  Value *returnedCond = evalValue(args->cons->car->cons->car, env, globalEnv);
  // printValue(returnedCond);
  // printf(" returnedCond\n");

  if(returnedCond->type == booleanType){
    if(returnedCond->booleanValue == true){ 

      if(args->cons->car->cons->cdr->cons == NULL){
        Value *returnVal = malloc(sizeof(Value));
        returnVal->type = booleanType;
        returnVal->booleanValue = true;
        return returnVal;
      }

      return evalValue(args->cons->car->cons->cdr->cons->car, env, globalEnv);
    }
    else{

      return evalCond(args->cons->cdr, env, globalEnv);
    }
  }
  
  if(args->cons->car->cons->cdr->cons == NULL){
    // printf("got here\n");
    return returnedCond;
    // Value *returnVal = malloc(sizeof(Value));
    // returnVal->type = booleanType;
    // returnVal->booleanValue = true;
    // return returnVal;
  }
  
  
  return evalValue(args->cons->car->cons->cdr->cons->car, env, globalEnv);

  //return (Value *) NULL;
  
}

Value *evalAnd(Value *args, Environment *env, Environment **globalEnv){
  
  if(args->cons == NULL){
    Value *returnVal = malloc(sizeof(Value));
    returnVal->type = booleanType;
    returnVal->booleanValue = true;
    return returnVal;
  }
  assert(args->cons->car != NULL);
  Value *returnedAnd = evalValue(args->cons->car, env, globalEnv);
  // printValue(returnedAnd);
  // printf("\n");
  if(returnedAnd->type == booleanType){
    if(returnedAnd->booleanValue == true){
      evalAnd(args->cons->cdr, env, globalEnv);
    }
    else{
      Value *returnVal = malloc(sizeof(Value));
      returnVal->type = booleanType;
      returnVal->booleanValue = false;
      return returnVal;
    }
  }
  else if(args->cons->cdr->cons == NULL){
    return returnedAnd;
  }

  return evalAnd(args->cons->cdr, env, globalEnv);
  
}

Value *evalOr(Value *args, Environment *env, Environment **globalEnv){
  
  if(args->cons == NULL){
    Value *returnVal = malloc(sizeof(Value));
    returnVal->type = booleanType;
    returnVal->booleanValue = false;
    return returnVal;
  }
  assert(args->cons->car != NULL);
  Value *returnedOr = evalValue(args->cons->car, env, globalEnv);

  if(returnedOr->type == booleanType){
    if(returnedOr->booleanValue == true){
      Value *returnVal = malloc(sizeof(Value));
      returnVal->type = booleanType;
      returnVal->booleanValue = true;
      return returnVal;
      
    }
    else{
      assert(args->cons->cdr != NULL);
      return evalOr(args->cons->cdr, env, globalEnv);
    }
  }

  return returnedOr;
  
}


Value *evalPlus(Value *args, float sum, VALUE_TYPE type, Environment *env, Environment **globalEnv){
  // base case
  if(args->cons == NULL){

    Value *returnSum = malloc(sizeof(Value));
    returnSum->copied = 1;
    if(type == integerType){
      returnSum->type = integerType;
      returnSum->integerValue = (int) sum;
    }
    else{
      returnSum->type = floatType;
      returnSum->floatValue = sum;
    }

    return returnSum; // freeeee
  } else { // recursive case
    Value *currentArg = evalValue(args->cons->car, env, globalEnv);

    if(currentArg->type == integerType){
      sum += currentArg->integerValue;
    }
    else if(currentArg->type == floatType){
      type = floatType; // only switch to float type if you have to
      sum += currentArg->floatValue;
    }
    else{ evaluationError(8); } // we've encountered an arg that's not add-able 
      
    return evalPlus(args->cons->cdr, sum, type, env, globalEnv); 
  }
}

Value *evalMult(Value *args, float product, VALUE_TYPE type, Environment *env, Environment **globalEnv){
  // base case
  if(args->cons == NULL){

    Value *returnProduct = malloc(sizeof(Value));
    returnProduct->copied = 1;
    if(type == integerType){
      returnProduct->type = integerType;
      returnProduct->integerValue = (int) product;
    }
    else{
      returnProduct->type = floatType;
      returnProduct->floatValue = product;
    }

    return returnProduct; // freeeee
  } else { // recursive case
    Value *currentArg = evalValue(args->cons->car, env, globalEnv);

    if(currentArg->type == integerType){
      product = product*(currentArg->integerValue);
    }
    else if(currentArg->type == floatType){
      type = floatType; // only switch to float type if you have to
      product = product*(currentArg->floatValue);
    }
    else{ evaluationError(8); } // we've encountered an arg that's not add-able 

    return evalMult(args->cons->cdr, product, type, env, globalEnv); 
  }
}

// Code body for evaluating <=, >=, =, >, <
Value *evalBoolStmt(ConsCell *expr, int boolOpValue, Environment *env, Environment **globalEnv){
  // Make sure the function received exactly 2 arguments
      if(lengthOfList(expr) != 3){ evaluationError(7); }

      // Get the first and second args after "<="
      Value *arg1 = evalValue(expr->cdr->cons->car, env, globalEnv);
      Value *arg2 = evalValue(expr->cdr->cons->cdr->cons->car, env, globalEnv);
      // malloc a return Value struc of type bool
      Value *boolResult = malloc(sizeof(Value));
      boolResult->copied = 1;
      boolResult->type = booleanType;

      // Make sure both args are numbers (either ints or floats)
      if (arg1->type != integerType && arg1->type != floatType){ evaluationError(5); }
      if (arg2->type != integerType && arg2->type != floatType){ evaluationError(6); }

      double arg1val = getDouble(arg1);
      double arg2val = getDouble(arg2);

      switch(boolOpValue){
        case leq:
          if (arg1val <= arg2val) {boolResult->booleanValue = true; }
          else {boolResult->booleanValue = false; }
          break;

        case geq:
          if (arg1val >= arg2val) {boolResult->booleanValue = true; }
          else {boolResult->booleanValue = false; }
          break;

        case eq:
          if (arg1val == arg2val) {boolResult->booleanValue = true; }
          else {boolResult->booleanValue = false; }
          break;

        case lt:
          if (arg1val < arg2val) {boolResult->booleanValue = true; }
          else {boolResult->booleanValue = false; }
          break;

        case gt:
          if (arg1val > arg2val) {boolResult->booleanValue = true; }
          else {boolResult->booleanValue = false; }
          break;
      }
    

      return boolResult;
}

int lengthOfList(ConsCell *list){
  int length = 0;
  while(list != NULL){
    length ++;
    if (list->cdr != NULL && list->cdr->type == consType) list = list->cdr->cons;  
    else list = NULL;
  }

  return length;
}

// takes a value and returns either that variable's value or that primitive's value
Value *evalValue(Value *value, Environment *env, Environment **globalEnv){
  switch(value->type){
    case integerType:
    case floatType:
    case booleanType:
    case stringType:

      return value;
      break;

    case symbolType:
      return resolveVariable(value, env, *globalEnv);
      break;

    case consType:
      return evalExpr(value->cons, env, globalEnv);
      break;

    case closureType:

      break;
  }

  return (Value *) NULL;
}

// currentEnvironment is a linked list, in this case, it serves as a stack
Environment *createEnvironment(ConsCell *varChain, Environment *oldEnvironment, Environment **globalEnv){
  assert(varChain != NULL);

  // Create a new frame and attach it to the old one
  ConsCell *newFrame = malloc(sizeof(ConsCell));
  newFrame->car = malloc(sizeof(Value));
  newFrame->car->type = consType;
  newFrame->cdr = malloc(sizeof(Value));
  newFrame->cdr->type = consType;
  newFrame->car->cons = NULL;
  newFrame->cdr->cons = oldEnvironment; // May have to cast as ConsCell
  Environment *currentEnvironment = newFrame;

  while(varChain != NULL){
    // make sure stuff exists
    assert(varChain->car != NULL);
    assert(varChain->car->cons != NULL);
    assert(varChain->car->cons->car != NULL);
    // identifier has to be symbolType because it can't be anything else
    assert(varChain->car->cons->car->type == symbolType); 
    
    // Get variable name
    char *currentVarName = varChain->car->cons->car->symbolValue;
    Value *currentVarValue;
    // Get variable value
    if(varChain->car->cons->cdr->cons->car->type != consType){
      currentVarValue = evalValue(varChain->car->cons->cdr->cons->car, currentEnvironment, globalEnv);
    } else {
      currentVarValue = evalExpr(varChain->car->cons->cdr->cons->car->cons, currentEnvironment, globalEnv);
    }

    // Create binding with that information
    Binding *currentBinding = malloc(sizeof(Binding));
    currentBinding->varName = currentVarName;
    currentBinding->value = currentVarValue;

    ConsCell *previousBindingCons = newFrame->car->cons;
    // Put new binding at the head of the linked list
    ConsCell *currentBindingCons = malloc(sizeof(ConsCell));
    currentBindingCons->car = malloc(sizeof(Value));
    currentBindingCons->car->type = bindingType;
    currentBindingCons->cdr = malloc(sizeof(Value));
    currentBindingCons->cdr->type = consType;
    
    currentBindingCons->car->binding = currentBinding;
    currentBindingCons->cdr->cons = previousBindingCons; 
    newFrame->car->cons = currentBindingCons;

    if(currentVarValue->type == closureType){
      freeTree(currentVarValue->closure->frame);
      currentVarValue->closure->frame = currentEnvironment;
    }
    // Check to see if we have any more variables to bind and iterate if so.
    if (varChain->cdr != NULL){
      varChain = varChain->cdr->cons;  
    } else {
      varChain = NULL;
    }
  }

  return currentEnvironment;
}


// currentEnvironment is a linked list, in this case, it serves as a stack
Environment *createLetStarEnvironment(ConsCell *varChain, Environment *oldEnvironment, Environment **globalEnv){
  assert(varChain != NULL);

  Environment *currentEnvironment = oldEnvironment;

  while(varChain != NULL){

    // Create a new frame and attach it to the old one
    ConsCell *newFrame = malloc(sizeof(ConsCell));
    newFrame->car = malloc(sizeof(Value));
    newFrame->car->type = consType;
    newFrame->cdr = malloc(sizeof(Value));
    newFrame->cdr->type = consType;
    newFrame->car->cons = NULL;
    newFrame->cdr->cons = currentEnvironment; // May have to cast as ConsCell

  
    // make sure stuff exists
    assert(varChain->car != NULL);
    assert(varChain->car->cons != NULL);
    assert(varChain->car->cons->car != NULL);
    // identifier has to be symbolType because it can't be anything else
    assert(varChain->car->cons->car->type == symbolType); 
    
    // Get variable name
    char *currentVarName = varChain->car->cons->car->symbolValue;
    Value *currentVarValue;
    // Get variable value
    if(varChain->car->cons->cdr->cons->car->type != consType){
      currentVarValue = evalValue(varChain->car->cons->cdr->cons->car, currentEnvironment, globalEnv);
    } else {
      currentVarValue = evalExpr(varChain->car->cons->cdr->cons->car->cons, currentEnvironment, globalEnv);

    }

    // Create binding with that information
    Binding *currentBinding = malloc(sizeof(Binding));
    currentBinding->varName = currentVarName;
    currentBinding->value = currentVarValue;

    ConsCell *previousBindingCons = newFrame->car->cons;
    // Put new binding at the head of the linked list
    ConsCell *currentBindingCons = malloc(sizeof(ConsCell));
    currentBindingCons->car = malloc(sizeof(Value));
    currentBindingCons->car->type = bindingType;
    currentBindingCons->cdr = malloc(sizeof(Value));
    currentBindingCons->cdr->type = consType;
    
    currentBindingCons->car->binding = currentBinding;
    currentBindingCons->cdr->cons = previousBindingCons; 
    newFrame->car->cons = currentBindingCons;

    if(currentVarValue->type == closureType){
      freeTree(currentVarValue->closure->frame);
      currentVarValue->closure->frame = currentEnvironment;
    }
    // Check to see if we have any more variables to bind and iterate if so.
    if (varChain->cdr != NULL){
      varChain = varChain->cdr->cons;  
    } else {
      varChain = NULL;
    }

    currentEnvironment = newFrame;
  }

  return currentEnvironment;
}

// currentLetRecEnvironment is a linked list, in this case, it serves as a stack
Environment *createLetRecEnvironment(ConsCell *varChain, Environment *oldEnvironment, Environment **globalEnv){
  assert(varChain != NULL);

  // Create a new frame and attach it to the old one
  ConsCell *newFrame = malloc(sizeof(ConsCell));
  newFrame->car = malloc(sizeof(Value));
  newFrame->car->type = consType;
  newFrame->cdr = malloc(sizeof(Value));
  newFrame->cdr->type = consType;
  newFrame->car->cons = NULL;
  newFrame->cdr->cons = oldEnvironment; // May have to cast as ConsCell
  Environment *currentEnvironment = newFrame;

  while(varChain != NULL){
    // make sure stuff exists
    assert(varChain->car != NULL);
    assert(varChain->car->cons != NULL);
    assert(varChain->car->cons->car != NULL);
    // identifier has to be symbolType because it can't be anything else
    assert(varChain->car->cons->car->type == symbolType); 
    
    // Get variable name
    char *currentVarName = varChain->car->cons->car->symbolValue;
    Value *currentVarValue;
    // Get variable value

    // this is where the letrec stuff comes in
    // probs works since currentEnvironment is where the lambda will be created, and where it'll call itself
    if(varChain->car->cons->cdr->cons->car->type != consType){
      currentVarValue = evalValue(varChain->car->cons->cdr->cons->car, currentEnvironment, globalEnv);  
    } else {
      currentVarValue = evalExpr(varChain->car->cons->cdr->cons->car->cons, currentEnvironment, globalEnv);

    }

    // Create binding with that information
    Binding *currentBinding = malloc(sizeof(Binding));
    currentBinding->varName = currentVarName;
    currentBinding->value = currentVarValue;

    ConsCell *previousBindingCons = newFrame->car->cons;
    // Put new binding at the head of the linked list
    ConsCell *currentBindingCons = malloc(sizeof(ConsCell));
    currentBindingCons->car = malloc(sizeof(Value));
    currentBindingCons->car->type = bindingType;
    currentBindingCons->cdr = malloc(sizeof(Value));
    currentBindingCons->cdr->type = consType;

    
    currentBindingCons->car->binding = currentBinding;
    currentBindingCons->cdr->cons = previousBindingCons; 
    newFrame->car->cons = currentBindingCons;

    if(currentVarValue->type == closureType){
      freeTree(currentVarValue->closure->frame);
      currentVarValue->closure->frame = currentEnvironment;
    }
    // Check to see if we have any more variables to bind and iterate if so.
    if (varChain->cdr != NULL){
      varChain = varChain->cdr->cons;  
    } else {
      varChain = NULL;
    }
  }

  return currentEnvironment;
}

// Takes a value and copies it
Value* copyValue(Value *oldValue){

  assert(oldValue != NULL);
  Value* newValue = malloc(sizeof(Value));
  newValue->type = oldValue->type;

  int length;

  switch(oldValue->type){
    // Straightforward stuff
    case integerType:
      newValue->integerValue = oldValue->integerValue;
      break;
    case floatType:
      newValue->floatValue = oldValue->floatValue;
      break;
    case booleanType:
      newValue->booleanValue = oldValue->booleanValue;
      break;

    // Malloc'd stuff
    case stringType:
      assert(oldValue -> stringValue != NULL); // maybe if we really want to copy the value, even if this is null, it's ok?

      length = strlen(oldValue->stringValue);
      char* newString = malloc(sizeof(char)*(length+1)); // +1 for null terminator
      strcpy(newString, oldValue->stringValue);
      newValue->stringValue = newString;
      break;

    case symbolType:
      assert(oldValue->symbolValue != NULL); // same here?

      length = strlen(oldValue->symbolValue);
      char* newSymbol = malloc(sizeof(char)*(length+1)); // +1 for null terminator
      strcpy(newSymbol, oldValue->symbolValue);
      newValue->symbolValue = newSymbol;
      break;

    case consType:
      assert(oldValue != NULL);
      ConsCell *newCons = copyCons(oldValue->cons); //WEEEEIIRRRRDDDDDDDDD!!!!!!!!!

      newValue->cons = newCons;
      break;

    case closureType:
      newValue->closure = malloc(sizeof(Closure));
      newValue->closure->args = copyCons(oldValue->closure->args);
      newValue->closure->funcBody = copyCons(oldValue->closure->funcBody);
      newValue->closure->frame = copyCons(oldValue->closure->frame);

      break;

    case bindingType:
      newValue->binding = malloc(sizeof(Binding));
      char *newVarName = malloc(sizeof(char)*((strlen(oldValue->binding->varName)+1)));
      strcpy(newVarName, oldValue->binding->varName);
      newValue->binding->varName = newVarName;
      newValue->binding->value = copyValue(oldValue->binding->value);

      break;

    default:
      printf("something's wrongggg!!!\n");
  }
  newValue->copied = 1;
  return newValue;
}

ConsCell *copyCons(ConsCell *cons){
  if(cons == NULL) return (ConsCell*) NULL;

  ConsCell *newCons = malloc(sizeof(ConsCell));
  if (cons->car != NULL){ newCons->car = copyValue(cons->car); }
  else { newCons->car = NULL; }
  
  if (cons->cdr != NULL){ newCons->cdr = copyValue(cons->cdr); }
  else { newCons->cdr = NULL; }

  return newCons;
}

Environment* copyEnvironment(Environment *env){
  if(env == NULL) return env;
  Environment* newEnv = malloc(sizeof(Environment));
  if(env->car != NULL) { newEnv->car = copyValue(env->car); }
  else { newEnv->car = NULL; }

  if(env->cdr != NULL) { newEnv->cdr = copyValue(env->cdr); }
  else { newEnv->cdr = NULL; }

  return newEnv;

}

double getDouble(Value *val){
  assert(val->type == integerType || val->type == floatType);

  if (val->type == integerType){ return (double) val->integerValue; }
  else { return val->floatValue; }
}


Value* evalIf(ConsCell *args, Environment *env, Environment **globalEnv){
  // check if there are exactly 3 args
  if (args == NULL || args->cdr->cons == NULL || args->cdr->cons->cdr->cons == NULL) {evaluationError(9);}
  Value *cond = args->car;
  Value *conseq = args->cdr->cons->car;
  Value *alt = args->cdr->cons->cdr->cons->car;

  Value *condVal;
  if (cond->type == consType){
    condVal = evalExpr(args, env, globalEnv);
  }
  else{
    condVal = evalValue(cond, env, globalEnv);
  }

  if(condVal == NULL || condVal->type != booleanType){evaluationError(10);}

  //if conditional evaluates to true return conseq
  if(condVal->booleanValue){
    Value *conseqVal;
    
    // Determine which eval function to use
    if (conseq->type == consType){ conseqVal = evalExpr(args->cdr->cons, env, globalEnv); } 
    else { conseqVal = evalValue(conseq, env, globalEnv); }
 
    if(condVal->copied == 1) freeReturnValue(condVal);
    return conseqVal;
  }
  
  //if conditional evals to false, return alt
  else { 
    Value *altVal;
    
    // Determine which eval function to use
    if (alt->type == consType){ altVal = evalExpr(args->cdr->cons->cdr->cons, env, globalEnv);} 
    else { altVal = evalValue(alt, env, globalEnv); }

    if(condVal->copied == 1) freeReturnValue(condVal);
    return altVal;
  }

  return (Value *) NULL; // to satisfy control
}

// basically, we're just going to return the stuff
Value* evalQuote(Value *args){
  assert(args != NULL);
  return args;
}

void printEnvironment(Environment *env){
  if(env == NULL){ printf("Environment is empty. \n"); }
  int i = 0;
  while (env != NULL){
    i++; printf("Frame: %i\n", i );

    // search inside current environment for variable
    assert(env->car != NULL);
    assert(env->car->cons != NULL);
    ConsCell *currentBindingCons = env->car->cons;
    while (currentBindingCons != NULL){
      assert(currentBindingCons->car != NULL);
      assert(currentBindingCons->car->binding != NULL);
      // Get variable properties
      char *currentVarName = currentBindingCons->car->binding->varName;
      Value *currentVarValue = currentBindingCons->car->binding->value;
      printf("\tBinding:\n");
      printf("\t\tVariableName: %s\n", currentVarName);
      printf("\t\tVariableValType: %i\n", currentVarValue->type);
      printf("\t\tVariableValVal: ");
      printValue(currentVarValue);
      fflush(stdout);
      printf("\n\n");

      // Go down the linked list if something exists there
      if (currentBindingCons->cdr != NULL){
        currentBindingCons = currentBindingCons->cdr->cons;  
      } else {
        currentBindingCons = NULL;
      }
    }

    // go to the next frame if there is one
    if (env->cdr != NULL){
      env = env->cdr->cons; 
      printf("\n");
    } else {
      env = NULL;
    }
  }
}

Value* resolveVariable(Value* value, Environment *env, Environment *globalEnv){
  assert(env != NULL || globalEnv != NULL); // need to have an environment to find a variable in it
  assert(value != NULL);
  assert(value->type == symbolType);
  assert(value->symbolValue != NULL);

  char *variableName = value->symbolValue;

  // search environments for varable
  //think about maybe modularizing into another function for checking bindings
  while (env != NULL){
    // search inside current environment for variable
    assert(env->car != NULL);
    assert(env->car->cons != NULL);
    ConsCell *currentBindingCons = env->car->cons;

    while (currentBindingCons != NULL){
      assert(currentBindingCons->car->binding != NULL);

      // Get variable properties
      char *currentVarName = currentBindingCons->car->binding->varName;
      Value *currentVarValue = currentBindingCons->car->binding->value;

      // See it's the variable we're looking for
      if(variableName != NULL && !strcmp(currentVarName, variableName)){
        return currentVarValue;
      }

      // Go down the linked list if something exists there
      if (currentBindingCons->cdr != NULL){
        currentBindingCons = currentBindingCons->cdr->cons;  
      } else {
        currentBindingCons = NULL;
      }
    }

    // go to the next frame if there is one
    if (env->cdr != NULL){
      env = env->cdr->cons;  
    } else {
      env = NULL;
    }
  }

  if(globalEnv->car->cons != NULL){
    //check global frame
    assert(globalEnv->car->cons != NULL);
    ConsCell *currentBindingCons = globalEnv->car->cons;
    while (currentBindingCons != NULL){
      assert(currentBindingCons->car->binding != NULL);

      // Get variable properties
      char *currentVarName = currentBindingCons->car->binding->varName;
      Value *currentVarValue = currentBindingCons->car->binding->value;
      
      // See it's the variable we're looking for
      if(!strcmp(currentVarName, variableName)){
        return currentVarValue;
      }

      if (currentBindingCons->cdr != NULL){
        currentBindingCons = currentBindingCons->cdr->cons;  
      } else {
        currentBindingCons = NULL;
      }
      
    }
  }

  // didn't find a variable
  evaluationError(11);
  return (Value *) NULL; // to satisfy control requirement
}

void freeReturnValue(Value *val){
  switch(val->type){
    case symbolType:
        if(val->symbolValue != NULL) free(val->symbolValue);
        break;
    case stringType:
        if(val->stringValue != NULL) free(val->stringValue);
        break;
    
    case consType:
        if(val->cons != NULL){
            freeTree(val->cons);
        }
        break;

    // case bindingType:

    //   break;

    case closureType:
      if (val->closure == NULL) return;

      if (val->closure->args != NULL)
        freeTree(val->closure->args);
      if (val->closure->funcBody != NULL)
        freeTree(val->closure->funcBody);
      if (val->closure->frame != NULL)
        freeTree((ConsCell*) val->closure->frame); //FREE THE ENVIRONMENT

      if(val->closure != NULL)
        free(val->closure);

      break;

    default:
      break;
  }
  
  if(val != NULL && val->copied == 1) free(val);
}

void evaluationError(int i){
  printf("Evaluation Error: %i \n", i);
  exit(0);
}