/* tokenizer.h by Erin Wilson, Marielle Foster, and Adam Canady
   CS 251 - Dave Musicant - Winter 2014

   This file outlines the data structures and functions used 
   by tokenizer.c. */

#include <stdbool.h>

typedef enum {
  booleanType, //0
  integerType, //1
  floatType, //2
  stringType, //3
  symbolType, //4
  openType, //5
  closeType, //6
  consType, //7
  bindingType, //8
  closureType, //9
  voidType //10
} VALUE_TYPE;

typedef struct __ConsCell {
    struct __Value *car;
    struct __Value *cdr;
} ConsCell;

typedef ConsCell Environment;

// Maybe this could be a Node-like struct to hold all the bindings we find within a certain Frame?

typedef struct __Binding {
    char *varName; 
    struct __Value *value;
} Binding;

typedef struct __Closure {
  // (1) a list of formal parameter names; 
  ConsCell *args;
  // (2) a pointer to the function body; 
  ConsCell *funcBody; //funky body
  // (3) a pointer to the environment frame in which the function was created.
  Environment *frame;
} Closure;


typedef struct __Value {
    int type; /* Make sure type includes a consType or some such */
    int copied;
    union {
        bool booleanValue;
        int integerValue;
        double floatValue;
        char *stringValue;
        char *symbolValue;
        char openValue; // not sure if necessary
        char closeValue; // not sure if necessary
        ConsCell *cons;
        Binding *binding;
        Closure *closure;
    };
} Value;


// Function definitions
ConsCell *tokenize(char *current_line, ConsCell *current_head);
int next_char(char *current_line, int position);
void print_list(ConsCell *first_cell);
void cleanup_list(ConsCell *current_cell);
ConsCell* reverse_list(ConsCell *current_cell);
