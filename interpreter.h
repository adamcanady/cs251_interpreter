typedef enum {
  leq, //0
  geq, //1
  eq, //2
  lt, //3
  gt //4
} BOOL_OP;

Value* evalExpr(ConsCell *expr, Environment *env, Environment **globalEnv);
Value* evalValue(Value *value, Environment *env, Environment **globalEnv);
Value* resolveVariable(Value *variable, Environment *env, Environment *globalEnv);
Value* evalIf(ConsCell *args, Environment *env, Environment **globalEnv);
Value* evalQuote(Value *args);
Environment *createEnvironment(ConsCell *newVar, Environment *current_environment, Environment **globalEnv);
Environment *createLetStarEnvironment(ConsCell *varChain, Environment *oldEnvironment, Environment **globalEnv);
Environment *createLetRecEnvironment(ConsCell *varChain, Environment *oldEnvironment, Environment **globalEnv);
void evaluationError(int i);
int lengthOfList(ConsCell *list);
Value *copyValue(Value *oldValue);
Value *evalPlus(Value *args, float sum, VALUE_TYPE type, Environment *env, Environment **globalEnv);
Environment* copyEnvironment(Environment *env);
Environment *createLambdaEnv(int numActualParameters, ConsCell* funcArgs, ConsCell* actualParameters, Environment* localEnvironment, Environment* env, Environment **globalEnv);
Value *evaluateLambda(Value *closureVal, ConsCell *expr, Environment *env, Environment **globalEnv);
Environment *cleanOneFrame(Environment *env);
void printEnvironment(Environment *env);
// void addToEnd(ConsCell** list, ConsCell *end);
ConsCell *copyCons(ConsCell *cons);
Value *evalMult(Value *args, float sum, VALUE_TYPE type, Environment *env, Environment **globalEnv);
Value *evalCond(Value *args, Environment *env, Environment **globalEnv);
double getDouble(Value *val);
Value *evalAnd(Value *args, Environment *env, Environment **globalEnv);
Value *evalOr(Value *args, Environment *env, Environment **globalEnv);
Value *evalBoolStmt(ConsCell *expr, int boolOpValue, Environment *env, Environment **globalEnv);
void freeReturnValue(Value *val);
