;; Adam Canady and Max Willard
;; CS 251 - Dave Musicant - Winter 2014

;; bst.rkt - Implementation of a Binary Search Tree

(define null-bst (lambda  )) ;; return an empty bst

(define null-bst? ;; check to see if a bst is null
  (lambda (bst)
    (if (equal? (null-bst) bst)
        #t
        #f
    )
  )
)

(define entry ;; return the root element not in a list
  (lambda (bst)
    (cond ((null-bst? bst) #f)
          ((not (list? (car (cdr bst)))) #f)
          ((not (list? (car (cdr (cdr bst))))) #f)
          (else (car bst))
    )
  )
)  

(define left ;; return the left subtree of given bst node
  (lambda (bst)
    (cond ((null-bst? bst) #f)
          ((not (list? (car (cdr bst)))) #f)
          ((not (list? (car (cdr (cdr bst))))) #f)
          (else (car (cdr bst)))
    )
  )
)

(define right ;; return the right subtree of a given bst node
  (lambda (bst)
    (cond ((null-bst? bst) #f)
          ((not (list? (car (cdr bst)))) #f)
          ((not (list? (car (cdr (cdr bst))))) #f)
          (else (car (cdr (cdr bst))))
    )
  )
)

(define make-bst ;; make a bst given a number and two subtrees
  (lambda (elt left right)
    (cond ((not (and (number? elt) (list? left) (list? right) )) #f) ;; not a number, left not a list, or right not a list
          ((not (and (or (and (null? left)) ;; check to see if left side is correct
                         (and (= 3 (length left))
                              (list? (car (cdr left)))
                              (list? (car (cdr (cdr left))))
                         )
                     )
                     (or (null? right) ;; check to see if right side is correct
                         (and (= 3 (length right)) 
                              (list? (car (cdr right)))
                              (list? (car (cdr (cdr right))))
                         ) ;; l/r either (null) or (3 in length and last two elements are lists)
                     )
                )
            ) #f) ;; lists not 3 in length and right and left last two elements are list OR l/r are null
          (else (list elt left right))
    )
  )
)

(define preorder 
  (lambda (bst)
    (cond ((and (null-bst? (left bst)) (null-bst? (right bst))) (list (entry bst))) ;; just the root if both leaves are empty
          ((null-bst? (left bst)) (append (list (entry bst)) (preorder (right bst)))) ;; just the root, then right leaf if left is empty
          ((null-bst? (right bst)) (append (list (entry bst)) (preorder (left bst)))) ;; just the root and left leaf if right is empty
          (else (append (list (entry bst)) (preorder (left bst)) (preorder (right bst)))) ;; root, left, and right if they all exist
    )
  )
)

(define inorder 
  (lambda (bst)
    (cond ((and (null-bst? (left bst)) (null-bst? (right bst))) (list (entry bst))) ;; just the root if left and right don't exist
          ((null-bst? (left bst)) (append (list (entry bst)) (inorder (right bst)))) ;; just the root and right if left doesn't exist
          ((null-bst? (right bst)) (append (inorder (left bst)) (list (entry bst)))) ;; just the left and root if right doesn't exist
          (else (append (inorder (left bst)) (list (entry bst)) (inorder (right bst)))) ;; left, root, and right if they all exist
    )
  )
)

(define postorder 
  (lambda (bst)
    (cond ((and (null-bst? (left bst)) (null-bst? (right bst))) (list (entry bst))) ;; just the root if left and right don't exist
          ((null-bst? (left bst)) (append (postorder (right bst)) (list (entry bst)))) ;; just the right and root if left doesn't exist
          ((null-bst? (right bst)) (append (postorder (left bst)) (list (entry bst)))) ;; just the left and root if right doesn't exist
          (else (append (postorder (left bst)) (postorder (right bst)) (list (entry bst)))) ;; left, right, and root if they all exist
    )
  )
)

(define insert
  (lambda (v bst)
    (cond ((= v (entry bst)) bst) ;; if they're equal, just return the bst
          ((and (< v (entry bst)) (null-bst? (left bst))) (make-bst (entry bst) (make-bst v (null-bst) (null-bst)) (right bst))) ;; v less than entry and empty leaf to put it in
          ((< v (entry bst)) (make-bst (entry bst) (insert v (left bst)) (right bst))) ;; v less than entry but need to check another level
          ((and (> v (entry bst)) (null-bst? (right bst))) (make-bst (entry bst) (left bst) (make-bst v (null-bst) (null-bst)))) ;; v greater than entry and empty leaf to put it in
          ((> v (entry bst)) (make-bst (entry bst) (left bst) (insert v (right bst)))) ;; v greater than entry but need to check another level
    )
  )
)