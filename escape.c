/* Implementation of the escape and unescape functions */
// Structure by Dave Musicant. Function Implementations by Adam Canady
// CS 251 - Winter 2014

#include <stdlib.h>
#include <string.h> 
#include <stdio.h>
#include "escape.h"

/* This method replaces special characters in the string with their
   escaped two-character versions. */
char *escape(char *unescaped) {
  // determine length of unescaped string
  int length_unescaped = strlen(unescaped);

  // make a new string with size of one char
  char* escaped = malloc(sizeof(char));

  int new_index = 0;
  // loop through unescaped string
  for(int i = 0; i<length_unescaped; i++){
    switch(unescaped[i]){
      case '\n':
        escaped = realloc(escaped, sizeof(char)*(new_index+2));
        escaped[new_index] = '\\';
        new_index++;
        escaped[new_index] = 'n';
        new_index++;
        break;
      case '\t':
        escaped = realloc(escaped, sizeof(char)*(new_index+2));
        escaped[new_index] = '\\';
        new_index++;
        escaped[new_index] = 't';
        new_index++;
        break;
      case '\\':
        escaped = realloc(escaped, sizeof(char)*(new_index+2));
        escaped[new_index] = '\\';
        new_index++;
        escaped[new_index] = '\\';
        new_index++;
        break;
      case '\'':
        escaped = realloc(escaped, sizeof(char)*(new_index+2));
        escaped[new_index] = '\\';
        new_index++;
        escaped[new_index] = '\'';
        new_index++;
        break;
      case '\"':
        escaped = realloc(escaped, sizeof(char)*(new_index+2));
        escaped[new_index] = '\\';
        new_index++;
        escaped[new_index] = '\"';
        new_index++;
        break;
      default:
        escaped = realloc(escaped, sizeof(char)*(new_index+1));
        escaped[new_index] = unescaped[i];
        new_index++;
        break;
    }
  }

  // Add end character
  escaped = realloc(escaped, sizeof(char)*(new_index+1));
  escaped[new_index] = '\0';
  return escaped;
}

/* This method replaces escaped characters with their one-character
   equivalents. */
char *unescape(char *escaped) {
  // determine length of unescaped string
  int length_escaped = strlen(escaped);

  // make a new string
  char* unescaped = malloc(sizeof(char)); // Allocate one character for our new string
  // new string length

  int new_index = 0;
  // loop through escaped string
  for(int i = 0; i<length_escaped; i++){
    // Add two since we're one greater than the index and adding one character on top of that
    unescaped = realloc(unescaped, sizeof(char)*(new_index+2));

    if(escaped[i] == '\\'){ // if the current character is an escape char
      switch(escaped[i+1]){ // do different stuff based on what the next char is
          case 'n':
            unescaped[new_index] = '\n';
            new_index++;
            i++;
            break;
          case 't':
            unescaped[new_index] = '\t';
            new_index++;
            i++;
            break;
          case '\\':
            unescaped[new_index] = '\\';
            new_index++;
            i++;
            break;
          case '\'':
            unescaped[new_index] = '\'';
            new_index++;
            i++;
            break;
          case '\"':
            unescaped[new_index] = '\"';
            new_index++;
            i++;
            break;
      }
    } else {
      unescaped[new_index] = escaped[i];
      new_index++;
    }
  }

  // Add space for last character (one greater than index, one space for null terminator)
  unescaped = realloc(unescaped, sizeof(char)*(new_index+2));
  // Add null terminator
  // printf("Index: %i\n", new_index);
  unescaped[new_index] = '\0';
  return unescaped;
}