#include "tokenizer.h"
#include "parser.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    ConsCell *tokens = getTokens(); // linked list returned by tokenizer
    ConsCell *tree = NULL; // Dis be the stack
    int depth = 0;

    while (tokens != NULL) {
        ConsCell *token;
        popFromFront(&tokens,&token);
        addToParseTree(&tree,&token,&depth);
        if (depth == 0) {
            printTree(tree);
            printf("\n");
            freeTree(tree);
            tree = NULL;
        }
    }    

    if (depth != 0) {
        syntaxError("Too Many Open Parens."); // error case 4, too many open parens
    }
}