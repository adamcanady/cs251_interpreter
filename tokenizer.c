/* tokenizer.c by Erin Wilson, Marielle Foster, and Adam Canady
   CS 251 - Dave Musicant - Winter 2014
   This program takes an input file and tokenizes it into a linked list.

   Example usage: clang -g tokenizer.c -o tokenizer
                  cat schemetest.rkt | ./tokenizer */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h> // for isdigit('2') and isalpha('a')

#include "tokenizer.h"
#include "escape.h"

/* Takes the index of the last character we know exists, and skips whitespace until we find the next character.
   If we don't find another character (all whitespace), we return the length of the line+1.*/
int next_char(char *current_line, int position){
    // if the next index is still in the line
    // and the next index is not a newline
    // and the next index is not a tab
    if(position+1 < strlen(current_line) 
        && *(current_line+position+1) != '\n'){
        //&& *(current_line+position+1) != '\t'){
        for(int i = position+1; i < strlen(current_line); i++){
            if(*(current_line+i) != ' ' && *(current_line+i) != '\t'){ return i; }
        }   
    }
    return strlen(current_line)+1;
}

/** tokenize a line, adding tokens to a linked list structure 
returns the most recent tokenized node 
naming note: "token" refers to a Value struct, "head" refers to a ConsCell struct*/
ConsCell* tokenize(char *current_line, ConsCell *current_head){
    int current_line_length = strlen(current_line);
    int window_start = 0; // start of the current token
    int window_end = 0; // end of the current token

    // get window_start to the first character's index // **********
    window_start = next_char(current_line, window_end-1);
    // printf("Window_start: %i\n", window_start);

    // shift window until we can't anymore
    while (window_start < current_line_length){ // loop until we get to the end of a line // ***** double check if it should be <= or not ***
        window_end = window_start; // assign the end to the start (token of length 1 to start out with)

        // check for comment (;)
        if (current_line[window_start] == ';') return current_head;

        // if we only have whitespace left, return head // ******************

        Value *new_token = malloc(sizeof(Value));
        new_token->copied = 0;
        
        bool found_token = false; // begin look for token mode
        bool in_string = false;
        
        while (!found_token) {// loop until we get to the end of a token and have found a full token
           
            // allocate space in the stack for a string long enough for our current search window and a null terminator
            char *window = malloc(sizeof(char) * (window_end-window_start+2));
            
            strncpy(window, current_line+window_start, window_end-window_start+1);
           
            *(window+(window_end-window_start+1)) = '\0'; // null terminate
            int window_length = strlen(window);
            

            /* If we're currently in a string, we have to determine that and react accorindgly */
            if (in_string){
                // ****double check to remove double quotes*****
                if (current_line[window_end] == '"' && current_line[window_end-1] != '\\'){ 
                    in_string = false;
                    
                    // Make token with current start/end
                    new_token->type = stringType;
                    new_token->stringValue = unescape(window);
                    
                    //substring from window_start to window_end
                    window_start = next_char(current_line, window_end); // ******also get rid of all white space*******
                    found_token = true;
                    free(window);
                    continue;
                } else if (window_end >= current_line_length){ // if this is the last character of the line
                    printf("Syntax Error: String not closed.\n");
                    free(window);
                    cleanup_list(current_head);
                    exit(0);
                } else {
                    window_end++;
                    free(window);
                    continue; // skip everything below
                }
            }

            // log("Place 1");

            /* If our search window is only length 1, the only tokens that can come out of ONLY that are open or close
               or we can find the start of a string */
            if (window_length == 1){
                switch(*window){
                    case '"':
                        in_string = true;
                        window_end++; // expand search window
                        free(window);
                        continue;
                    case '(':
                        new_token->type = openType;
                        new_token->openValue = '(';
                        window_start = next_char(current_line, window_end);
                        found_token = true;
                        free(window);
                        continue; // loop around again
                    case ')':
                        new_token->type = closeType;
                        new_token->openValue = ')';
                        window_start = next_char(current_line, window_end);
                        found_token = true;
                        free(window);
                        continue; // loop around again
                    case '\'':
                        new_token->type = symbolType;
                        new_token->symbolValue = window;
                        window_start = next_char(current_line, window_end);
                        found_token = true;
                        continue; // loop around again
                }
            }

            /* If our search window is only length 2, we must check for #t or #f and create boolean tokens
               accordingly. */
            if (window_length == 2){
                if(strcmp(window, "#t") == 0){ // true
                    new_token->type = booleanType;
                    new_token->booleanValue = true;
                    window_start = next_char(current_line, window_end);
                    found_token = true;
                    free(window);
                    continue;
                } else if(strcmp(window, "#f") == 0){ // false
                    new_token->type = booleanType;
                    new_token->booleanValue = false;
                    window_start = next_char(current_line, window_end);
                    found_token = true;
                    free(window);
                    continue;
                }
            }

            // log("Place 3");

            /* If we got here, it means we've determined that the current search window
               is not an open or close, it's not part of a string, so it must be an int, float,
               or symbol. */

            /* Here we check to see if we've reached the end of the int, float or symbol, by checking
               if the last character of our search_window is a token ender (open, close, or whitespace)*/
            // printf("%s\n", window+window_length-1);
            // printf("%s\n", window+window_length-1);
            if(*(window+window_length-1) == ' ' || 
               *(window+window_length-1) == '(' || 
               *(window+window_length-1) == ')' || 
               *(window+window_length-1) == ';' ||
               *(window+window_length-1) == '"' ||

               window_end >= current_line_length){ // if it's the last character of the line, that also ends the token
                // decide what type it is (integer, float, or other (symbol))
                bool is_integer = true;
                bool is_float = true;

                // We know that the last character in the window is not part of this token
                // so take it off 
                if(!(window_end >= current_line_length)) {
                    window[window_length-1] = '\0';
                    window_length--;
                }

                bool one_period = false; // In case someone comes up with stupid variable names i.e. 5..2
                int i = 0;
                if (window[i] == '-' && window_length > 1) { i++; }
                while(i < window_length){
                    if(window[i] == '.'){ // has a period, must not be integer
                        is_integer = false;
                        
                        if (one_period) {
                            is_float = false;
                        } else {
                            one_period = true;
                        }
                    } else if(!isdigit(window[i])){ // has a character that's not a '.', can't be a number
                        is_integer = false;
                        is_float = false;
                    }
                    i++;
                }

                if(is_integer){ // found an integer token!
                    new_token->type = integerType;
                    new_token->integerValue = atoi(window); // valgrind complains here
                    window_start = next_char(current_line, window_end-1);
                    found_token = true;
                } else if(is_float){ // found a float token!
                    new_token->type = floatType;
                    new_token->floatValue = atof(window); // valgrind complains here
                    window_start = next_char(current_line, window_end-1);
                    found_token = true;
                } else { // must be a symbol
                    new_token->type = symbolType;
                    new_token->symbolValue = window;
                    window_start = next_char(current_line, window_end-1);
                    found_token = true;
                    continue;
                }

            } else { // If we have not reached the end of the int, float, or symbol, we should increase the window size
                window_end++;
            }

            free(window);
        }

        // Update linked list with new token that we just found, assigning head to that token
        // and having it point to the previously found token
        Value *old_token = malloc(sizeof(Value));
        old_token->copied = 0;
        old_token->type = consType;
        old_token->cons = current_head; 

        ConsCell *new_head = malloc(sizeof(ConsCell));
        new_head->car = new_token;
        new_head->cdr = old_token;

        current_head = new_head;
       
    }

    return current_head;
}

//reverses a linked list, and then returns the new first item in the linked list
ConsCell* reverse_list(ConsCell *current_cell){
    ConsCell *reversed_head = NULL;
    while(current_cell){
        ConsCell *next_cell = current_cell->cdr->cons;
        current_cell->cdr->cons = reversed_head;
        reversed_head = current_cell;
        current_cell = next_cell;
    }
    return reversed_head;
}

void print_list(ConsCell *first_cell){
    while(first_cell != NULL){
        switch(first_cell->car->type){
            case integerType:
                printf("%i:integer\n", first_cell->car->integerValue);
                break;
            case floatType:
                printf("%f:float\n", first_cell->car->floatValue);
                break;
            case symbolType:
                printf("%s:symbol\n", first_cell->car->symbolValue); // valgrind complains here
                break;
            case stringType:
                printf("%s:string\n", first_cell->car->stringValue);
                break;
            case booleanType:
                printf("%s:boolean\n", first_cell->car->booleanValue?"true":"false");
                break;
            case openType:
                printf("(:open\n");
                break;
            case closeType:
                printf("):close\n");
                break;
        }

        first_cell = first_cell->cdr->cons;
    }

}

void cleanup_list(ConsCell *current_cell){
    // while cleaning up, make sure to free pointers if they are symbol, cons, or string type
    ConsCell *next_cell;
    while(current_cell){
        switch(current_cell->car->type){
            
            case symbolType:
                next_cell = current_cell -> cdr-> cons;
                free(current_cell->car->symbolValue);
                free(current_cell->car);
                free(current_cell->cdr);
                free(current_cell);
                current_cell = next_cell;
                break;
            case stringType:
                next_cell = current_cell -> cdr-> cons;
                free(current_cell->car->stringValue);
                free(current_cell->car);
                free(current_cell->cdr);
                free(current_cell);
                current_cell = next_cell;
                break;
            case consType:
                next_cell = current_cell -> cdr-> cons;
                free(current_cell->car->cons); // this could also be recursive maybe?
                free(current_cell->car);
                free(current_cell->cdr);
                free(current_cell);
                current_cell = next_cell;
                break;

            default:
                next_cell = current_cell -> cdr-> cons;
                free(current_cell->car);
                free(current_cell->cdr);
                free(current_cell);
                current_cell = next_cell;
                break;
        }
    
    }
}