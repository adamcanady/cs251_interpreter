#include "tokenizer.h"
#include "parser.h"
#include "interpreter.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){
    ConsCell *tokens = getTokens(); // linked list returned by tokenizer
    ConsCell *tree = NULL; // Dis be the stack
    int depth = 0;
    
    Environment *globalEnv = malloc(sizeof(Environment));
    globalEnv->car = malloc(sizeof(Value));
    globalEnv->car->cons = NULL;
    globalEnv->cdr = NULL; // never matters EVER

    while (tokens != NULL) {
        ConsCell *token;
        popFromFront(&tokens,&token);
        addToParseTree(&tree,&token,&depth);
        if (depth == 0) {
            Value *returnedValue;
            // evaluate tree (what if tree's cdr is NULL -- as in program just contains '1')
            if(tree->car->type == consType){
                returnedValue = evalExpr(tree->car->cons, NULL, &globalEnv);
            } else {
                returnedValue = evalValue(tree->car, NULL, &globalEnv);
            }
            printValue(returnedValue);

            if(returnedValue != NULL) printf("\n");
            if(returnedValue != NULL && returnedValue->copied == 1) freeReturnValue(returnedValue);
            freeTree(tree);
            tree = NULL;
        }
    }    

    //free global frame here
    freeBindings(globalEnv->car->cons);
    // free(globalEnv->car->cons); 
    free(globalEnv->car);
    free(globalEnv);

    if (depth != 0) {
        syntaxError("Too Many Open Parens."); // error case 4, too many open parens
    }
    exit(0);
}