# Makefile by Erin Wilson, Marielle Foster, and Adam Canady
# CS 251 - Dave Musicant - Winter 2014

## Main
main: interpreter

## Tokenizer
tokenizer: tokenizer_main.o tokenizer.o escape.o
	clang -g tokenizer_main.o tokenizer.o escape.o -o tokenizer

tokenizer_main.o: tokenizer_main.c
	clang -c -g tokenizer_main.c

tokenizer.o: tokenizer.c
	clang -c -g tokenizer.c

escape.o: escape.c
	clang -c -g escape.c


## Parser
parser: parser_main.o parser.o tokenizer.o escape.o
	clang -g parser_main.o parser.o tokenizer.o escape.o -o parser

parser_main.o: parser_main.c
	clang -c -g parser_main.c

parser.o: parser.c
	clang -c -g parser.c


## Interpreter
interpreter: interpreter_main.o interpreter.o parser.o tokenizer.o escape.o
	clang -g interpreter_main.o interpreter.o parser.o tokenizer.o escape.o -o interpreter

interpreter_main.o: interpreter_main.c
	clang -g -c interpreter_main.c

interpreter.o: interpreter.c
	clang -g -c interpreter.c


## Diffs
tokenizer-diff: tokenizer
	cat tokenizer-test.input.01 | ./tokenizer | diff tokenizer-test.output.01 - && cat tokenizer-test.input.02 | ./tokenizer | diff tokenizer-test.output.02 - && cat tokenizer-test.input.03 | ./tokenizer | diff tokenizer-test.output.03 - && cat tokenizer-test.input.04 | ./tokenizer | diff tokenizer-test.output.04 - && cat tokenizer-test.input.05 | ./tokenizer | diff tokenizer-test.output.05 - && cat tokenizer-test.input.06 | ./tokenizer | diff tokenizer-test.output.06 - && cat tokenizer-test.input.07 | ./tokenizer | diff tokenizer-test.output.07 - && cat tokenizer-test.input.08 | ./tokenizer | diff tokenizer-test.output.08 -

tokenizer-test-update: tokenizer
	cat tokenizer-test.input.01 | ./tokenizer > tokenizer-test.output.01 && cat tokenizer-test.input.02 | ./tokenizer > tokenizer-test.output.02 && cat tokenizer-test.input.03 | ./tokenizer > tokenizer-test.output.03 && cat tokenizer-test.input.04 | ./tokenizer > tokenizer-test.output.04 && cat tokenizer-test.input.05 | ./tokenizer > tokenizer-test.output.05 && cat tokenizer-test.input.06 | ./tokenizer > tokenizer-test.output.06 && cat tokenizer-test.input.07 | ./tokenizer > tokenizer-test.output.07 && cat tokenizer-test.input.08 | ./tokenizer > tokenizer-test.output.08

parser-diff: parser
	cat parser-test.input.01 | ./parser | diff parser-test.output.01 - && cat parser-test.input.02 | ./parser | diff parser-test.output.02 - && cat parser-test.input.03 | ./parser | diff parser-test.output.03 - && cat parser-test.input.04 | ./parser | diff parser-test.output.04 - && cat parser-test.input.05 | ./parser | diff parser-test.output.05 - && cat parser-test.input.06 | ./parser | diff parser-test.output.06 - && cat parser-test.input.07 | ./parser | diff parser-test.output.07 - && cat parser-test.input.08 | ./parser | diff parser-test.output.08 -

parser-test-update: parser
	cat parser-test.input.01 | ./parser > parser-test.output.01 && cat parser-test.input.02 | ./parser > parser-test.output.02 && cat parser-test.input.03 | ./parser > parser-test.output.03 && cat parser-test.input.04 | ./parser > parser-test.output.04 && cat parser-test.input.05 | ./parser > parser-test.output.05 && cat parser-test.input.06 | ./parser > parser-test.output.06 && cat parser-test.input.07 | ./parser > parser-test.output.07 && cat parser-test.input.08 | ./parser > parser-test.output.08

interpreter-diff: interpreter
	cat interpreter-test.input.01 | ./interpreter | diff interpreter-test.output.01 - && cat interpreter-test.input.02 | ./interpreter | diff interpreter-test.output.02 - && cat interpreter-test.input.03 | ./interpreter | diff interpreter-test.output.03 - && cat interpreter-test.input.04 | ./interpreter | diff interpreter-test.output.04 - && cat interpreter-test.input.05 | ./interpreter | diff interpreter-test.output.05 - && cat interpreter-test.input.06 | ./interpreter | diff interpreter-test.output.06 - && cat interpreter-test.input.07 | ./interpreter | diff interpreter-test.output.07 - && cat interpreter-test.input.08 | ./interpreter | diff interpreter-test.output.08 -

interpreter-test-update: interpreter
	cat interpreter-test.input.01 | ./interpreter > interpreter-test.output.01 && cat interpreter-test.input.02 | ./interpreter > interpreter-test.output.02 && cat interpreter-test.input.03 | ./interpreter > interpreter-test.output.03 && cat interpreter-test.input.04 | ./interpreter > interpreter-test.output.04 && cat interpreter-test.input.05 | ./interpreter > interpreter-test.output.05 && cat interpreter-test.input.06 | ./interpreter > interpreter-test.output.06 && cat interpreter-test.input.07 | ./interpreter > interpreter-test.output.07 && cat interpreter-test.input.08 | ./interpreter > interpreter-test.output.08

interpreter2-diff: interpreter
	cat interpreter2-test.input.01 | ./interpreter | diff interpreter2-test.output.01 - && cat interpreter2-test.input.02 | ./interpreter | diff interpreter2-test.output.02 - && cat interpreter2-test.input.03 | ./interpreter | diff interpreter2-test.output.03 - && cat interpreter2-test.input.04 | ./interpreter | diff interpreter2-test.output.04 - && cat interpreter2-test.input.05 | ./interpreter | diff interpreter2-test.output.05 - && cat interpreter2-test.input.06 | ./interpreter | diff interpreter2-test.output.06 - && cat interpreter2-test.input.07 | ./interpreter | diff interpreter2-test.output.07 - && cat interpreter2-test.input.08 | ./interpreter | diff interpreter2-test.output.08 -

interpreter2-test-update: interpreter
	cat interpreter2-test.input.01 | ./interpreter > interpreter2-test.output.01 && cat interpreter2-test.input.02 | ./interpreter > interpreter2-test.output.02 && cat interpreter2-test.input.03 | ./interpreter > interpreter2-test.output.03 && cat interpreter2-test.input.04 | ./interpreter > interpreter2-test.output.04 && cat interpreter2-test.input.05 | ./interpreter > interpreter2-test.output.05 && cat interpreter2-test.input.06 | ./interpreter > interpreter2-test.output.06 && cat interpreter2-test.input.07 | ./interpreter > interpreter2-test.output.07 && cat interpreter2-test.input.08 | ./interpreter > interpreter2-test.output.08

interpreter3-diff: interpreter
	cat interpreter3-test.input.01 | ./interpreter | diff interpreter3-test.output.01 - && cat interpreter3-test.input.02 | ./interpreter | diff interpreter3-test.output.02 - && cat interpreter3-test.input.03 | ./interpreter | diff interpreter3-test.output.03 - && cat interpreter3-test.input.04 | ./interpreter | diff interpreter3-test.output.04 - && cat interpreter3-test.input.05 | ./interpreter | diff interpreter3-test.output.05 - && cat interpreter3-test.input.06 | ./interpreter | diff interpreter3-test.output.06 - && cat interpreter3-test.input.07 | ./interpreter | diff interpreter3-test.output.07 - && cat interpreter3-test.input.08 | ./interpreter | diff interpreter3-test.output.08 -

interpreter3-test-update: interpreter
	cat interpreter3-test.input.01 | ./interpreter > interpreter3-test.output.01 && cat interpreter3-test.input.02 | ./interpreter > interpreter3-test.output.02 && cat interpreter3-test.input.03 | ./interpreter > interpreter3-test.output.03 && cat interpreter3-test.input.04 | ./interpreter > interpreter3-test.output.04 && cat interpreter3-test.input.05 | ./interpreter > interpreter3-test.output.05 && cat interpreter3-test.input.06 | ./interpreter > interpreter3-test.output.06 && cat interpreter3-test.input.07 | ./interpreter > interpreter3-test.output.07 && cat interpreter3-test.input.08 | ./interpreter > interpreter3-test.output.08


## Valgrind
valgrind: interpreter_main.c interpreter.c parser.c tokenizer.c escape.c
	clang -g interpreter_main.c interpreter.c parser.c tokenizer.c escape.c -o interpreter

valgrind-tokenizer: tokenizer
	cat ${file} | valgrind --leak-check=full ./tokenizer

valgrind-parser: parser
	cat ${file} | valgrind --leak-check=full ./parser

valgrind-interpreter: interpreter
	cat ${file} | valgrind --leak-check=full ./interpreter

valgrind-interpreter-all: interpreter
	cat interpreter2-test.input.01 | valgrind --leak-check=full ./interpreter 1> grep SUMMARY 
	cat interpreter2-test.input.02 | valgrind --leak-check=full ./interpreter 1> grep SUMMARY 
	cat interpreter2-test.input.03 | valgrind --leak-check=full ./interpreter 1> grep SUMMARY 
	cat interpreter2-test.input.04 | valgrind --leak-check=full ./interpreter 1> grep SUMMARY 
	cat interpreter2-test.input.05 | valgrind --leak-check=full ./interpreter 1> grep SUMMARY 
	cat interpreter2-test.input.06 | valgrind --leak-check=full ./interpreter 1> grep SUMMARY 
	cat interpreter2-test.input.07 | valgrind --leak-check=full ./interpreter 1> grep SUMMARY 
	cat interpreter2-test.input.08 | valgrind --leak-check=full ./interpreter 1> grep SUMMARY

## Misc
test: valgrind
	make valgrind && cat interpreter-test.input.01 | valgrind --leak-check=full valgrind --leak-check=full ./interpreter | grep -A 7 "LEAK SUMMARY|ERROR SUMMARY" *.o *.dSYM tokenizer parser interpreter tokenizer_main parser_main interpreter_main

clean: 
	rm -rf *.o *.dSYM tokenizer parser interpreter tokenizer_main parser_main interpreter_main
